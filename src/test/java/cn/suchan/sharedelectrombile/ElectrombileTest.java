package cn.suchan.sharedelectrombile;

import cn.suchan.sharedelectrombile.entity.Electrombile;
import cn.suchan.sharedelectrombile.repository.ElectrombileRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Optional;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ElectrombileTest {

    @Autowired
    private ElectrombileRepository electrombileRepository;

    @Test
    public void saveTest() throws ParseException {
        Electrombile electrombile = new Electrombile();
        DateFormat dateFormat;
        dateFormat = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss", Locale.ENGLISH);//设定格式
        //dateFormat = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss", Locale.ENGLISH);
        dateFormat.setLenient(false);
        Date timeDate = dateFormat.parse("2018-07-24 15:10:20");//util类型
        Timestamp dateTime = new java.sql.Timestamp(timeDate.getTime());//Timestamp类型,timeDate.getTime()返回一个long型

        electrombile.setPlateNumber("桂BA888888");
        electrombile.setType("小绵羊");
        electrombile.setProductionDate(dateTime);

        System.out.println("save==>" + electrombileRepository.save(electrombile));
    }

    @Test
    public void findByIdTest(){
        Optional<Electrombile> electrombile = electrombileRepository.findById(2);
        System.out.println("electrombile.get()==>>" + electrombile.get());
        System.out.println("electrombile.toString()==>>" + electrombile.toString());
        System.out.println("electrombile.hashCode()==>>" + electrombile.hashCode());
        System.out.println("electrombile.isPresent()==>>" + electrombile.isPresent());

        //System.out.println("null的時候==>>" + electrombileRepository.findById(3).get());
        electrombile.ifPresent(System.out::println);
        electrombile.orElse(null);
        System.out.println("Optional.of(electrombile)==>" + Optional.of(electrombile));

    }
}
