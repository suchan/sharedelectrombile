/*创建数据库*/
create database db_electrombile default charset utf8;
/*使用数据库*/
use db_electrombile;
/*创建用户表*/
create table tb_user(uid int not null primary key auto_increment, uname varchar(50),password varchar(255),realname varchar(20),phonenumber varchar(255),email varchar(255));
/*创建角色表*/
create table tb_role(rid int not null primary key auto_increment, rname varchar(50));
/*创建权限表*/
create table tb_resource(rsid int not null primary key auto_increment, rsname varchar(50),rsdescribe varchar(255));
