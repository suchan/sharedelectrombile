package cn.suchan.sharedelectrombile.response;

import lombok.Builder;
import lombok.Data;

public enum ResponseCode {

    SUCCESS(1,"请求成功"),
    FAIL(0,"请求失败"),
    WARN(-1,"网络异常，请稍候重试");

    private int code;
    private String msg;

    ResponseCode(int code,String msg){
        this.code = code;
        this.msg = msg;
    }

    public int getCode(){
        return code;
    }

    public String getMsg(){
        return msg;
    }
}
