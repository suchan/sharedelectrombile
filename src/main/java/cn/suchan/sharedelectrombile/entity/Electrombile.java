package cn.suchan.sharedelectrombile.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Past;
import java.sql.Timestamp;
import java.util.Set;

@Table(name = "tb_electrombile")
@Entity
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Electrombile {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer eid;

    @NotBlank(message = "车牌号不能为空")
    private String plateNumber;

    @NotBlank(message = "类型不能为空")
    private String type;

    @Past
    private Timestamp productionDate;

    @OneToMany(mappedBy = "electrombile")
    private Set<UsageLog> usageLogSet;
}
