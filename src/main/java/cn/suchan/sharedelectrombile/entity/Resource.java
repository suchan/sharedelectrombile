package cn.suchan.sharedelectrombile.entity;

import lombok.Builder;
import lombok.Data;

import javax.persistence.*;
import java.util.Set;

@Table(name = "tb_resource")
@Entity
@Builder
@Data
public class Resource {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer rsid;
    private String rsname;
    private String rsdescribe;

    @ManyToMany(mappedBy = "resourceSet")
    private Set<Role> roleSet;
}
