package cn.suchan.sharedelectrombile.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Builder;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.Set;

@Table(name = "tb_user")
@Entity
@Builder
@Data
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer uid;

    @NotBlank(message = "用户名不能为空")
    private String uname;

    @NotBlank(message = "密码不能为空")
    @Size(min = 6,message = "密码长度不少于六位")
    private String password;

    @Pattern(regexp = "^[\\u4e00-\\u9fa5]{2,6}$")
    @Size(min = 2,max = 6)
    private String realname;

    @Pattern(regexp = "^1(3|4|5|7|8)\\d{9}$",message = "手机号码格式错误")
    private String phone;

    private String address;

    @Email(message = "邮箱格式错误")
    private String email;

    @OneToMany(mappedBy = "user")
    private Set<UsageLog> usageLogSet;

    @ManyToMany(mappedBy = "userSet")
    private Set<Role> roleSet;
}
