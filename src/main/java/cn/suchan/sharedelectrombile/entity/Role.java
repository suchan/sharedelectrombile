package cn.suchan.sharedelectrombile.entity;

import lombok.Builder;
import lombok.Data;

import javax.persistence.*;
import java.util.Set;

@Table(name = "tb_role")
@Entity
@Builder
@Data
public class Role {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer rid;
    private String rname;

    @ManyToMany
    @JoinTable(name = "tb_role_user",joinColumns = {@JoinColumn(name = "uid")},
            inverseJoinColumns = {@JoinColumn(name = "rid")})
    private Set<User> userSet;

    @ManyToMany
    @JoinTable(name = "tb_role_resource",joinColumns = {@JoinColumn(name = "rsid")},
            inverseJoinColumns = {@JoinColumn(name = "rid")})
    private Set<Resource> resourceSet;
}
