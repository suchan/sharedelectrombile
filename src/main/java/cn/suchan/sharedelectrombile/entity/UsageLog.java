package cn.suchan.sharedelectrombile.entity;

import lombok.Builder;
import lombok.Data;

import javax.persistence.*;
import java.sql.Timestamp;

@Table(name = "tb_usagelog")
@Entity
@Builder
@Data
public class UsageLog {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer ulid;
    //原先停放的地点
    private String preaddress;
    //现在停放的地点
    private String sufadress;
    //骑走时间
    private Timestamp departureTime;
    //停放时间
    private Timestamp arrivalTime;
    //消费金额
    private Double cost;
    //车辆状态（使用中，维修中，已废弃）
    private String state;

    @ManyToOne
    private User user;

    @ManyToOne
    private Electrombile electrombile;

}
