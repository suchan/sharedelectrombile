package cn.suchan.sharedelectrombile;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SharedelectrombileApplication {

    public static void main(String[] args) {
        SpringApplication.run(SharedelectrombileApplication.class, args);
    }
}
