package cn.suchan.sharedelectrombile.controller;

import cn.suchan.sharedelectrombile.entity.Electrombile;
import cn.suchan.sharedelectrombile.response.Response;
import cn.suchan.sharedelectrombile.service.ElectrombileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/electrombile")
public class ElecttombileController {

    @Autowired
    private ElectrombileService electrombileService;

    @Autowired
    private AppController appController;

    /**
     * 共享电动车管理员
     */

    /**
     * 增加/修改电动车信息
     */
    /*@GetMapping("/save")*/
    @PostMapping("/save")
    public List<Response> saveElectrombile(@RequestBody @Valid Electrombile electrombile, BindingResult bindingResult){
        List<Response> responseList = new ArrayList<>();
        System.out.println("electrombile==>" + electrombile.toString());

        return appController.saveAndDeleteValid(bindingResult,electrombileService.save(electrombile));
    }

    /**
     * 删除电动车信息
     */
    @PostMapping("/delete")
    public List<Response> delete(@RequestBody @Valid Electrombile electrombile,BindingResult bindingResult){
        List<Response> responseList = new ArrayList<>();
        System.out.println("electrombile==>" + electrombile.toString());

        return appController.saveAndDeleteValid(bindingResult,electrombileService.delete(electrombile));
    }
}
