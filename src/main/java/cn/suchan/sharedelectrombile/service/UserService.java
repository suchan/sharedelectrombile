package cn.suchan.sharedelectrombile.service;

import cn.suchan.sharedelectrombile.entity.User;
import cn.suchan.sharedelectrombile.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    /**
     * 根据用户名和密码查询用户角色id(可能有多个对应的角色)
     */
    public List<Integer> getRidByUnameAndUpassword(User user){
        return userRepository.getRidByUnameAndUpassword(user.getUname(),user.getPassword());
    }
}
