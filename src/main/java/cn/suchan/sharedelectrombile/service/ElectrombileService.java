package cn.suchan.sharedelectrombile.service;

import cn.suchan.sharedelectrombile.entity.Electrombile;
import cn.suchan.sharedelectrombile.repository.ElectrombileRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ElectrombileService {

    @Autowired
    private ElectrombileRepository electrombileRepository;

    public boolean save(Electrombile electrombile){
        Electrombile e = electrombileRepository.save(electrombile);
        if (e == null) {
            return false;
        }
        return true;
    }

    public boolean delete(Electrombile electrombile){
        electrombileRepository.delete(electrombile);
        Optional<Electrombile> electrombile1 = electrombileRepository.findById(electrombile.getEid());
        if (electrombile1.isPresent()){
            return false;
        }
        return true;
    }
}
