package cn.suchan.sharedelectrombile.service;

import cn.suchan.sharedelectrombile.entity.Role;
import cn.suchan.sharedelectrombile.repository.RoleRepositroy;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoleService {

    @Autowired
    private RoleRepositroy roleRepositroy;

    public List<Integer> findByRid(Integer rid){
        return roleRepositroy.findByRid(rid);
    }

}
