package cn.suchan.sharedelectrombile.repository;

import cn.suchan.sharedelectrombile.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User,Integer> {

    /**
     * 根据用户名和密码查询用户角色id(可能有多个对应的角色)
     */
    @Query(value = "select ru.rid from tb_role_user as ru,tb_user as u where ru.uid=u.uid and uname=?1 and password=?2",nativeQuery = true)
    public List<Integer> getRidByUnameAndUpassword(String uname, String password);
}
