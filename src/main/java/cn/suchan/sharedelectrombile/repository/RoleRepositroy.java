package cn.suchan.sharedelectrombile.repository;

import cn.suchan.sharedelectrombile.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RoleRepositroy extends JpaRepository<Role, Integer> {

    /**
     * 根据rid查询相应的rsid
     */
    @Query(value = "select rsid from tb_role_resource where rid = ?1",nativeQuery = true)
    List<Integer> findByRid(Integer rid);
}
