package cn.suchan.sharedelectrombile.repository;

import cn.suchan.sharedelectrombile.entity.UsageLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsageLogRepository extends JpaRepository<UsageLog,Integer> {
}
