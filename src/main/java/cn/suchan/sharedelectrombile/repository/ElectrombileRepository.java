package cn.suchan.sharedelectrombile.repository;

import cn.suchan.sharedelectrombile.entity.Electrombile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ElectrombileRepository extends JpaRepository<Electrombile,Integer> {
}
